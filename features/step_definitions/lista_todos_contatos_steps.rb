Dado('que faço um GET na API') do #para listar um unico contato basta incluir "/9999" no final da URL
  @url = 'https://api-de-tarefas.herokuapp.com/contacts/'        
  @headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/vnd.tasksmanager.v2'
    }
    @response = HTTParty.get(@url, headers: @headers )# :body => {"name":"AAA","last-name": "BBB", "email":"oteb66@oteb66.com","age":"123","phone":"123123","address":"AAA","state":"Sao Paulo","city":"Santos"} )
    
end

Então('recebo um resultado') do
    #log @response.code 
    #log @response
     @code_return = 200
    #@body_return = @response.body
    
    log @response["data"][5]
    raise "Erro: o codigo retornado esta errado" if @code_return != @response.code
    raise "A API esta retornando vazio" if @response["data"] == nil
    $ident_contat = @response["data"][5]["id"]
end