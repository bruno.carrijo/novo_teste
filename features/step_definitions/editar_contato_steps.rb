
Dado('que faço um PUT na API para editar um contato') do
    steps %(
        Dado que faço um GET na API
        Então recebo um resultado
    )

    #@url = "https://api-de-tarefas.herokuapp.com/contacts/#{$ident_contat}"       
    @headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.tasksmanager.v2'
      }
      @response = HTTParty.put("#{BASE_URL['url_base']}#{$ident_contat}", headers: @headers, :body => { 
        "last_name":Faker::Name.last_name,
        "email":Faker::Internet.email,
        }.to_json )

end

Então('recebo a confirmaçao de sucesso') do
    @code_return = 200
    log @response
    raise "Erro: o codigo retornado esta errado" if @code_return != @response.code
    log $ident_contat
   
end