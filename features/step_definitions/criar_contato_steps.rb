Dado('que faço um POST na API passando os parametros') do

  @headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/vnd.tasksmanager.v2'
    }
    @response = HTTParty.post(BASE_URL["url_base"], headers: @headers, :body => {"name":Faker::Name.first_name,
                                                                 "last_name":Faker::Name.last_name,
                                                                 "email":Faker::Internet.email,
                                                                 "age":"35",
                                                                 "phone":Faker::PhoneNumber.phone_number,
                                                                 "address":Faker::Address.street_name,
                                                                 "state":Faker::Address.state,
                                                                 "city":Faker::Address.city}.to_json )
    
end

Então('recebo um resultado de sucesso') do
    #log @response.body
    #log @response.body["data"]["id"]
    $novo_contato = @response["data"]["id"] #atribuir dados do novo contato criado para uma variavel GLOBAL
    @url = "https://api-de-tarefas.herokuapp.com/contacts/#{$novo_contato}" #Usando o GET para verificar o novo contato criado
    @response = HTTParty.get(@url, headers: @headers)
    log @response
    log $novo_contato
    # @code_return = 201
    # raise "Erro: o codigo retornado esta errado" if @code_return != @response.code

end