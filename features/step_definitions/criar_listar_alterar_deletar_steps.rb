Dado('que eu crio um contato e listo esse contato criado') do
    @response = HTTParty.post(BASE_URL['url_base'], headers: headers_dados, body: criar_dados_contato.to_json )
    
    $novo_contato = @response["data"]["id"] #atribuir dados do novo contato criado para uma variavel GLOBAL 
    @code_return = 201
    raise "Erro: o codigo retornado esta errado" if @code_return != @response.code

    #listar contato criado
    @response = HTTParty.get("#{BASE_URL['url_base']}#{$novo_contato}", headers: headers_dados)

end

Quando('eu edito esse contato') do      
  
    @response = HTTParty.put("#{BASE_URL['url_base']}#{$novo_contato}", headers: headers_dados, body: editar_dados_contato.to_json)
    @code_return = 200
    raise "Erro: o codigo retornado esta errado" if @code_return != @response.code

end

Então('eu posso apagar o contato criado') do     
  
      @response = HTTParty.delete("#{BASE_URL['url_base']}#{$novo_contato}", headers: headers_dados)
      @response_retorno = HTTParty.get("#{BASE_URL['url_base']}#{$novo_contato}", headers: headers_dados)
      raise "Erro: o codigo retornado esta errado" if @response.code != 204
      raise "Erro: o codigo retornado esta errado" if @response_retorno.code != 404
    
end