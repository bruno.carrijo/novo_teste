Dado('que faço um DELETE na API') do
    steps %(
        Dado que faço um GET na API
        Então recebo um resultado
    )

    @url = "https://api-de-tarefas.herokuapp.com/contacts/#{$ident_contat}"       
    @headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.tasksmanager.v2'
      }
      @response = HTTParty.delete(@url, headers: @headers)
      @response_retorno = HTTParty.get(@url, headers: @headers)

end

Então('recebo a confirmaçao de contato deletado') do
    log @response
    raise "Erro: o codigo retornado esta errado" if @response.code != 204
    raise "Erro: o codigo retornado esta errado" if @response_retorno.code != 404

   
end