Before do
   
    Faker::Config.locale = 'pt-BR'
  $mail_faker = Faker::Internet.email
  $name_faker = Faker::Name.first_name
  $last_name_faker = Faker::Name.last_name
  $email_faker = Faker::Internet.email
  $age_faker = "35"
  $phone_faker = Faker::PhoneNumber.phone_number
  $address_faker = Faker::Address.street_name 
  $state_faker = Faker::Address.state
  $city_faker = Faker::Address.city


end

def criar_dados_contato    
    {
        "name": $name_faker,
        "last_name": $last_name_faker,
        "email": $email_faker,
        "age": $age_faker,
        "phone": $phone_faker,
        "address": $address_faker,
        "state": $state_faker,
        "city": $city_faker
    }
end

def editar_dados_contato 
    {
        "last_name": $last_name_faker,
        "email": $email_faker
    }
end

def headers_dados
   {
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.tasksmanager.v2'
    }
    
end